<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::POST('/user/create', [AuthController::class, 'store']);
Route::POST('/user/login', [AuthController::class, 'login']);
Route::POST('/user/check', [AuthController::class, 'check_data']);
Route::GET('/user/logout', [AuthController::class, 'logout']);
Route::GET('/verify/{email}/{token}', [AuthController::class, 'verify_email'])->name('verify-me');