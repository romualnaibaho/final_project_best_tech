<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    public function store(Request $request)
    {
        $validation = $request->validate([
            'name' => 'required|max:25|min:5',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x]).*$/',
        ]);

        if($validation)
        {
            $creation = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'token' => 'user-'.substr(str_shuffle($request->name.$request->email), 0, 25)
            ]);
    
            if($creation)
            {
                $user = User::where('email', $request->email)->first();
    
                $details = [
                    'name' => $user->name,
                    'email' => $user->email,
                    'token' => $user->token
                ];
               
                \Mail::to($user->email)->send(new \App\Mail\VerifyEmail($details));
    
                return response()->json([
                    'message'=>'success'
                ]);
            }
        }

        return response()->json([
            'message'=>'failed',
            'data'=> $creation
        ]);
    }

    public function check_data(Request $request)
    {
        $request->validate([
            'name' => 'required|max:25|min:5',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x]).*$/',
        ]);
    }

    public function verify_email($email, $token)
    {
        
        User::where('email', $email)->update([
            'is_active' => 1
        ]);
        
        return redirect('/login');
    }

    public function login(Request $request)
    {
        $validator = $request->validate([
            'email' => 'email|exists:users,email',
            'password' => 'required',
        ]);

        $attemps = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        $user = User::where('email', $request->email)->first();
        if($user->is_active == 1)
            {
                $login_process = \Auth::attempt($attemps, (bool) $request->remember);

            if($login_process)
            {
                return response()->json([
                    'message'=>'success',
                    'user' => $user
                ]);
            }
            else
            {
                return response()->json([
                    'message'=>'failed',
                    'error' => ["password"=>["Your password is incorrect."]]
                ]);
            }
        }else{
            return response()->json([
                'message'=>'failed',
                'error' => ["message"=>["You need to verify your email first"]]
            ]);
        }
    }

    public function logout()
    {
        \Auth::logout();

        return response()->json([
            'message'=>'success'
        ]);
    }
}
